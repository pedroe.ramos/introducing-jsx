//Esta funcion devuelve el estado
function getMood() {
  //Esta matriz tiene todos los estados de animo que se usaran
  const moods = ["Angry", "Hungry", "Silly", "Quiet", "Paranoid"]
  //Este return tomar la matriz del estado de ánimo y la longitud para encontrar un número aleatorio que se multiplique por el largo
  return moods[Math.floor(Math.random() * moods.length)]
}
class JSXDEMO extends React.Component {
  render() {
    return (
      <div>
        <h1>My current mood is: {getMood()}</h1>
      </div>
    )
  }
}

ReactDOM.render(<JSXDEMO />, document.getElementById("root"))
